package com.example.staticappproperties2.config;


public class StaticAllYml {
    public static ApplicationYml APP_PROP_CONFIG;

    public static MessageYml MESSAGE_PROP_CONFIG;

    public static ErrorMessageYml ERROR_MESSAGE_PROP_CONFIG;

    public static void setAppPropConfig(ApplicationYml applicationYml) {
        StaticAllYml.APP_PROP_CONFIG = applicationYml;
    }

    public static void setMessagePropConfig(MessageYml messageYml) {
        StaticAllYml.MESSAGE_PROP_CONFIG = messageYml;
    }

    public static void setErrorMessagePropConfig(ErrorMessageYml errorMessageYml) {
        StaticAllYml.ERROR_MESSAGE_PROP_CONFIG = errorMessageYml;
    }
}
