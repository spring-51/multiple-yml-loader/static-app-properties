package com.example.staticappproperties2.config;

import com.example.staticappproperties2.dto.Student;
import com.example.staticappproperties2.factory.YamlPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@Configuration
@PropertySource(value = "classpath:messages-en.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "my-msg")
public class MessageYml {
    @PostConstruct
    public void postConstruct(){
        StaticAllYml.setMessagePropConfig(this);
    }

    private String success;

    private Student student;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
