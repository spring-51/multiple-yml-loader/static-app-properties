package com.example.staticappproperties2.config;

import com.example.staticappproperties2.dto.Student;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@ConfigurationProperties(prefix = "my-app")
public class ApplicationYml {
    @PostConstruct
    public void postConstruct(){
        StaticAllYml.setAppPropConfig(this);
    }
    private String name;

    private Student student;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
