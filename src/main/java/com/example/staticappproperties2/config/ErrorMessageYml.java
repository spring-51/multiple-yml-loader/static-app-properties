package com.example.staticappproperties2.config;

import com.example.staticappproperties2.dto.Student;
import com.example.staticappproperties2.factory.YamlPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@Configuration
@PropertySource(value = "classpath:error-en.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "my-error")
public class ErrorMessageYml {

    @PostConstruct
    public void postConstruct(){
        StaticAllYml.setErrorMessagePropConfig(this);
    }

    private String ise;

    private Student student;

    public String getIse() {
        return ise;
    }

    public void setIse(String ise) {
        this.ise = ise;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
