package com.example.staticappproperties2;

import com.example.staticappproperties2.config.StaticAllYml;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaticAppProperties2Application implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(StaticAppProperties2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("############AppPropConfig################ "+ StaticAllYml.APP_PROP_CONFIG.getName());
        System.out.println("############AppPropConfig###Student############# "+ StaticAllYml.APP_PROP_CONFIG.getStudent().getId());

        System.out.println("############MessagePropConfig################ "+ StaticAllYml.MESSAGE_PROP_CONFIG.getSuccess());
        System.out.println("############MessagePropConfig###Student############# "+ StaticAllYml.MESSAGE_PROP_CONFIG.getStudent().getName());

        System.out.println("############ErrorMessagePropConfig################ "+ StaticAllYml.ERROR_MESSAGE_PROP_CONFIG.getIse());
        System.out.println("############ErrorMessagePropConfig###Student############# "+ StaticAllYml.ERROR_MESSAGE_PROP_CONFIG.getStudent().getFullname());
    }
}
